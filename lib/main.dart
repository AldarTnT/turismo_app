import 'package:flutter/material.dart';
import 'package:turismo_app/screens/screens.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget{
  const MyApp({super.key});
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Duwili Ella',
      home: const Scaffold(
        body: MenuOptions(),
      ),
      theme: ThemeData(
        textTheme: const TextTheme(
          displayLarge: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
            color: Colors.black
          )
        )
      ),
    );
  }
}