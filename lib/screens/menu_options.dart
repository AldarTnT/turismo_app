import 'package:flutter/material.dart';

class MenuOptions extends StatelessWidget{
  const MenuOptions({super.key});

  @override
  Widget build(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      height: 400,
            decoration: const BoxDecoration(
        gradient: LinearGradient(
          stops: [
            0.1,
            0.9
          ],
          colors: [
            Color(0xff4268D3),
            Color(0xff564ED1)
          ]
        )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const _TitleCards(),
          Container(
            width: double.infinity,
            height: 250,
            color: Colors.red,
            child: ListView(
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                  width: 200,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(20)
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                  width: 200,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(20)
                  ),
                )
                
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _BackgroundCards extends StatelessWidget {
  const _BackgroundCards({required this.size,});

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: size.height*0.35,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          stops: [
            0.1,
            0.9
          ],
          colors: [
            Color(0xff4268D3),
            Color(0xff564ED1)
          ]
        )
      ),
    );
  }
}

class _TitleCards extends StatelessWidget {
  const _TitleCards();

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      child: Text('Popular',style: Theme.of(context).textTheme.displayLarge,),
    ));
  }
}








